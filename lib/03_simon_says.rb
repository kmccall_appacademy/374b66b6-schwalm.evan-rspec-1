def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, n = 2)
  result = []

  i = 0
  while i < n
    result.push(str)
    i += 1
  end

  result.join(' ')
end

def start_of_word(str, n)
  substring = ""
  i = 0

  while i < n
    substring += str[i]
    i += 1
  end

  substring
end

def first_word(str)
  words = str.split(' ')
  words[0]
end

def titleize(str)
  title = []
  words = str.split(' ')

  i = 0
  while i < words.length

    if i == 0 && words[0] == 'the'
      title.push(words[i].capitalize)
    elsif words[i] == 'the' || words[i] == 'and' || words[i] == 'over'
      title.push(words[i])
    else
      title.push(words[i].capitalize)
    end
    i += 1
  end

  title.join(' ')
end

def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(numbers)
  sum = 0
  numbers.each { |number| sum += number }
  sum
end

def multiply(numbers)
  product = 1
  numbers.each { |number| product *= number }
  product
end

def power(base, exp)
  i = 0
  result = 1

  while i < exp
    result *= base
    i += 1
  end

  result
end

def factorial(number)
  if number == 0
    return 0
  end

  nums = 1..number
  result = 1
  nums.each { |n| result *= n }
  result
end

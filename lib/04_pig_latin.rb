def translate(str)
  words = str.split(' ')
  new_sen = []

  words.each { |word| new_sen.push(pig_latin(word)) }

  new_sen.join(' ')
end

def pig_latin(word)
  letters = word.split('')
  vowels = ['a', 'e', 'i', 'o', 'u']
  i = 0

  until vowels.include?(letters[i])
    if letters[i] == 'q' && letters[i + 1] == 'u'
      letters = letters.rotate
    end
    letters = letters.rotate
  end

  letters.join('') + 'ay'
end
